module.exports = function(grunt) {

  //Initializing the configuration object
    grunt.initConfig({

    // paths
    paths: {
      assets: {
        css: './public/assets/css/',
        js: './public/assets/js/',
      },
      vendor: './public/bower/',      
      css: './public/css/',
      js: './public/js/'

    },
      // Task configuration
    concat: {
      options: {
        separator: ';',
      },
      js_adminglobaldepend: {
        src: [
          '<%= paths.vendor %>jquery/dist/jquery.js',
          '<%= paths.vendor %>jquery-ui/jquery-ui.js',
          '<%= paths.vendor %>bootstrap/dist/js/bootstrap.js',
          '<%= paths.vendor %>angular/angular.js'
        ],
        dest: '<%= paths.js %>globaldepend.js'
      },

      js_adminPlugins: {
        src: [
          '<%= paths.assets.js %>plugins/morris/morris.js',
          '<%= paths.assets.js %>plugins/sparkline/jquery.sparkline.js',
          '<%= paths.assets.js %>plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
          '<%= paths.assets.js %>plugins/fullcalendar/fullcalendar.js',
          '<%= paths.assets.js %>plugins/jqueryKnob/jquery.knob.js',
          '<%= paths.assets.js %>plugins/daterangepicker/daterangepicker.js',
          '<%= paths.assets.js %>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js',
          '<%= paths.assets.js %>plugins/iCheck/icheck.js',
          '<%= paths.assets.js %>backend.js'
        ],
        dest: '<%= paths.js %>adminPlugins.js'
      },

      js_adminFrontend: {
        src: [
          '<%= paths.assets.js %>AdminLTE/app.js',
          '<%= paths.assets.js %>AdminLTE/dashboard.js',
          '<%= paths.assets.js %>AdminLTE/demo.js',
          '<%= paths.assets.js %>angular/adminApp.js',
          '<%= paths.assets.js %>angular/menuService.js',
          '<%= paths.assets.js %>angular/mainCtrl.js',

        ],
        dest: '<%= paths.js %>adminFrontend.js'
      },

      js_userglobalDepend: {
        src: [
        '<%= paths.vendor %>jquery/dist/jquery.min.js',
        '<%= paths.vendor %>bootstrap/dist/js/bootstrap.min.js',
        ],
        dest: '<%= paths.js %>userglobalDepend.js'
      },

      js_userFrontendDepend: {
        src: [
          '<%= paths.assets.js %>prettyPhoto.js',
          '<%= paths.assets.js %>jquery.isotope.min.js',
          '<%= paths.assets.js %>main.js',
          '<%= paths.assets.js %>wow.min.js',
        ],
        dest: '<%= paths.js %>userFrontendDepend.js'
      }

      
    },
    uglify: {
      options: {
        mangle: true  // Use if you want the names of your functions and variables unchanged
      },
      globaldepend: {
        files: {
          '<%= paths.js %>globaldepend.min.js': '<%= paths.js %>globaldepend.js',
        }
      },
      adminPlugins: {
        files: {
          '<%= paths.js %>adminPlugins.min.js': '<%= paths.js %>adminPlugins.js',
        }
      },
      adminFrontend: {
        files: {
          '<%= paths.js %>adminFrontend.min.js': '<%= paths.js %>adminFrontend.js',
        }
      },
      userglobalDepend: {
        files: {
          '<%= paths.js %>userFrontendDepend.min.js': '<%= paths.js %>userFrontendDepend.js',
        }
      },
      
    },
    phpunit: {
        classes: {
            dir: 'app/tests/'   //location of the tests
        },
        options: {
            bin: 'vendor/bin/phpunit',
            colors: true
        }
    },
    cssmin: {
  add_banner: {
    options: {
      banner: '/* My minified css file */',
      mangle: true
    },
    files: {
      '<%= paths.css %>final.min.css': ['<%= paths.assets.css %>**/*.css']
    }
  }
},
    watch: {
        js_frontend: {
          files: [
            //watched files
            '<%= paths.assets.js %>angular/adminApp.js',
            '<%= paths.assets.js %>angular/menuService.js',
            '<%= paths.assets.js %>angular/mainCtrl.js'
            ],   
          tasks: ['concat:js_adminFrontend','uglify:adminFrontend'],     //tasks to run
          options: {
            livereload: true                        //reloads the browser
          }
        },
        js_backend: {
          files: [
            //watched files
            '<%= paths.assets.vendor %>jquery/dist/jquery.js',
            '<%= paths.assets.vendor %>bootstrap/dist/js/bootstrap.js',
            '<%= paths.assets.js %>backend.js'
          ],   
          tasks: ['concat:js_backend','uglify:backend'],     //tasks to run
          options: {
            livereload: true                        //reloads the browser
          }
        },
        less: {
          files: ['<%= paths.assets.css %>*.less'],  //watched files
          tasks: ['less'],                          //tasks to run
          options: {
            livereload: true                        //reloads the browser
          }
        },
        tests: {
          files: ['laravel/app/controllers/*.php','laravel/app/models/*.php'],  //the task will run only when you save files in this location
          tasks: ['phpunit']
        }
      }
    });

  // Plugin loading
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-phpunit');

  // Task definition
  grunt.registerTask('default', ['watch']);

};