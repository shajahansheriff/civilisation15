<?php

class AdminController extends BaseController {

	public function __construct(){
		$this->beforeFilter('admin',array('only'=>'getIndex'));
		$this->beforeFilter('csrf',array('on'=>'post'));
	}
	/*Admin controller to handle the admin panel*/
 /**
  * [getIndex description]
  * @return [type]k
  */
	public function getIndex(){
		return View::make('admin.index');
	}
	
	public function getSignup(){
		return View::make('admin.signup');
	}

	/*public function postSignup(){
		$validator = Validator::make(Input::all(),User::$rules);

		if ($validator->passes()) {
			$user = new User;
			$user->name = Input::get('fname');
			$user->email = Input::get('email');
			$user->college = Input::get('college');
			$user->password = Hash::make(Input::get('password'));
			$user->admin = Input::get('admin');
			$user->save();

			return Redirect::to('c15/admin/signin')
				->with('message','Account created Successfully!. Please Signin to continue');
		}
		return Redirect::to('c15/admin/signup')
						->with('message','Account Not Created Successfully. Try Again')
						->withErrors($validator)
						->withInput();

	}
*/

	public function getCreatemenu(){
		return View::make('admin.create_menu');
	}

	public function getSignin(){
		if(Auth::check()) {
			return Redirect::intended();
		}
		return View::make('admin.signin');
		
	}

	public function postSignin(){
		
		$email = Input::get('email');
		$password = Input::get('password');
		if(Auth::attempt(array('email'=> $email,'password'=>$password)))
			{
				return Redirect::to('c15/admin')
					->with('message','Successfully Logged In');
			}
		return Redirect::to('c15/admin/signin')
			->with('message',"Please check you email and password!!");
	}

	public function getLogout(){
		Auth::logout();

		return Redirect::to('c15/admin');
	}

}