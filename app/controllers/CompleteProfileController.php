<?php

class CompleteProfileController extends \BaseController {

	public function __construct(){
		$this->beforeFilter('auth');
	}


	public function show(){
		if (Auth::user()->status == 1) {
			return Redirect::intended('/');
		}
		$id = Auth::user()->createdVia;
		switch ($id) {
			case 1:
				return View::make('users.moreinfo');
				break;
			
			default:
				return View::make('users.moreinfoFb');
				break;
		}
	}

	public function submit(){
		$validator = Validator::make(Input::all(),User::$moreRules);
		if ($validator->passes()) {
			$user = Auth::user();
			$user->city = ucwords(strtolower(Input::get('city')));
			$user->year = Input::get('year');
			$user->department = ucwords(strtolower(Input::get('department')));
			$user->gender = ucfirst(Input::get('gender'));
			$user->status = 1;
			$user->save();

			return Redirect::to('user/'.Auth::user()->cid)->with('message','Thanks for being part of C\'15. Your profile successfully updated!');
		}

		return Redirect::to('completeprofile')->with('message','Something went wrong!')
							->withErrors($validator)->withInput();

	}
	public function submitFb(){
		$validator = Validator::make(Input::all(),User::$moreRulesFb);
		if ($validator->passes()) {
			$user = Auth::user();
			$user->password = Hash::make(Input::get('password'));
			$user->college = ucwords(strtolower(Input::get('college')));
			$user->city = ucwords(strtolower(Input::get('city')));
			$user->year = Input::get('year');
			$user->department = ucwords(strtolower(Input::get('department')));
			$user->mobile = Input::get('mobile');
			$user->status = 1;
			$user->save();

			return Redirect::to('user/'.Auth::user()->cid)->with('message','Thanks for being part of C\'15. Your profile successfully updated!');
		}

		return Redirect::to('completeprofile')->with('message','Something went wrong!')
		->withErrors($validator)->withInput();

	}
}