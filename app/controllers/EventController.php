<?php

class EventController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$events = C15Event::all();
		return View::make('events')->with('events',$events);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
 		/*$name = Input::get('name');
 		$description = Input::get('description');
 		$rules = Input::get('rules');
 		$contact = Input::get('contact');
 		$category = Input::get('category');

 		$validate = Validator::make(Input::all(),C15Event::$rules);
 		if ($validate->passes()) {
 			$event = new C15Event;
 			$event->name = $name;
 			$event->description = $description;
 			$event->rules = $rules;
 			$event->contact = $contact;
 			$event->category_id = $category;
 			$event->save();

 			return 'success';
 		}

 		return $validate->errors();*/

		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$event = C15Event::find($id);
		return Response::json($event);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
