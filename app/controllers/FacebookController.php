<?php

class FacebookController extends \BaseController {
	
	public function getLogin(){
        if (Auth::check()) {
            return Redirect::to('user');
        }
	$facebook = new Facebook(Config::get('facebook'));
	$params = array(
		'redirect_uri'=>'http://localhost:8000/login/fb/callback',
		'scope'=>'email'
		);
	return Redirect::away($facebook->getLoginUrl($params));

}
public function getLoginCallback() {
    if (Auth::check()) {
        return Redirect::to('user');
    }

    $code = Input::get('code');
    if(strlen($code)==0){
    	Redirect::away('/')->with('message','Something Went Wrong. Please Try Again');
    }

    $facebook = new Facebook(Config::get('facebook'));
    $uid = $facebook->getUser();
    if ($uid == 0) {
    	Redirect::away('/')->with('message','Error Occured. Try Again');
    }
  
      $me = $facebook->api('/me');



    $profile = Profile::whereUid($uid)->first();
    if(empty($profile)){
        if (empty(User::where('email','=',$me['email'])->first())) {
        $user = new User;
        $user->name = ucwords(strtolower($me['first_name']));
        $user->email = strtolower($me['email']);
        $user->cid = Cid::getCId(User::all()->last());
        $user->image = 'https://graph.facebook.com/'.$me['id'].'/picture?type=large';
        $user->gender = ucfirst($me['gender']);
        $user->createdVia = 0;
        $user->status = 0;
        $user->save();  
        }
        else{
            $user = User::whereEmail($me['email'])->first();
        }
    	$profile = new Profile;
    	$profile->uid = $uid;
    	$profile->username = ucwords(strtolower($me['first_name'].' '.$me['last_name']));
    	$profile = $user->profiles()->save($profile);
    }
    $profile->access_token = $facebook->getAccessToken();
    $profile->save();
    $user = $profile->user;

    Auth::login($user);
    return Redirect::to('completeprofile')->with('message','Your account is successfully created!');

}
}