<?php

class MenuController extends \BaseController {
	/*public function __construct(){
		$this->beforeFilter('ajaxCheck');
	}*/

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		return Response::json(Menu::get());
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		
		$validator = Validator::make(Input::all(),Menu::$rules);
		//$err = var_dump($validator);
		if($validator->passes())
		{	
			$menu = new Menu;
			$menu->name = Input::get('name');
			$menu->save();

			return Response::json(array('success'=>true));
		}

		return Response::json(array('success'=>false));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		Menu::destroy($id);

		return Response::json(array('success'=>true));
	}


}
