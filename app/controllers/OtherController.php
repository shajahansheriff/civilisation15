<?php

class OtherController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function c14(){
		return View::make('others.c14');
	}

	public function root(){
		return View::make('index');
	}

	public function sponsors(){
		return View::make('others.sponsors');
	}

	public function about(){
		return View::make('others.aboutus');
	}

	public function workshops(){
		return View::make('comingsoon');
	}

	public function accomodation(){
		return View::make('comingsoon');
	}
	public function lectures(){
		return View::make('comingsoon');
	}

}
