<?php 

class SignInController extends \BaseController {
	
	public function index(){
		if (Auth::check()) {
			return Redirect::to('/');
		}
		return View::make('users.signin');
	}

	public function login(){
		$email = Input::get('email');
		$password = Input::get('password');
		$remember = Input::get('remember');
		if ($remember == 'true') {
			$remember = true;
		}
		else $remember = false;
		if(Auth::attempt(array('email'=>$email,'password'=>$password),$remember))
		{
			return Redirect::intended('completeprofile');
		}
		return Redirect::to('signin')->with('message','Incorrect email and password combination')
			->withInput();
	}
}