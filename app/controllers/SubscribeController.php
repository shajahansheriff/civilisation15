<?php

class SubscribeController extends \BaseController {
	public function __construct(){
		$this->beforeFilter('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$eventId = Input::get('event_id');
		$check = Subscribe::where('event_id',$eventId)->where('user_id',Auth::user()->id)->get();
		if (!empty($check)) {
		 	return 'success';
		 }
		return 'error';
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$eventId = Input::get('eventId');
		$check = Subscribe::where('event_id',$eventId)->where('user_id',Auth::user()->id)->first();
		if (empty($check)) {
			$subscribe = new Subscribe;
			$subscribe->event_id = $eventId;
			$subscribe->user_id = Auth::user()->id;
			$subscribe->save();

			return 'YES';
		}
		
		return 'NOT';
		
		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$eventId = $id;
		$check = Subscribe::where('event_id',$eventId)->where('user_id',Auth::user()->id)->first();
		if (empty($check)) {
		 	return 'NOT';
		 }
		return 'YES';
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$eventId = $id;
		$check = Subscribe::where('event_id',$eventId)->where('user_id',Auth::user()->id)->first();
		if (!empty($check)) {
			$check->delete();
			return 'YES';
		}
		return 'NOT';
	}


}
