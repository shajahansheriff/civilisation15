<?php
use app\libs;
class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct(){
		$this->beforeFilter('csrf',array('on'=>'POST'));
		$this->beforeFilter('auth',array('except'=>array('create','store')));
		$this->beforeFilter('profileCheck',array('except'=>array('create','store')));
	}

	public function index()
	{
		return Redirect::to('user/'.Auth::user()->cid);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if(Auth::check()) {
			return Redirect::to('user/'.Auth::user()->cid);
		}
		return View::make('users.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
		$validator = Validator::make(Input::all(), User::$rules);
		if ($validator->passes()) {
			$user = new User;
			$user->name = ucwords(strtolower(Input::get('name')));
			$user->email = strtolower(Input::get('email'));
			$user->college = ucwords(strtolower(Input::get('college')));
			$user->cid = Cid::getCId(User::all()->last());
			$user->password = Hash::make(Input::get('password'));
			$user->mobile = Input::get('mobile');
			$user->admin = 0;
			$user->createdVia = 1;
			$user->status = 0;
			$user->save();

			return Redirect::to('signin')->with('message','Your account created successfully!');
		}

		return Redirect::to('user/create')
			->with('message','Something is wrong.')
			->withErrors($validator)
			->withInput();
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::whereCid($id)->first();
		if (empty($user)) {
			return App::abort(404);
		}
		 return View::make('users.profile')->with('user',$user);			
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::whereCid($id)->first();
		if (Auth::user()->cid == $user->cid) {
		return View::make('users.edit')->with('user',$user);
		}
		return Redirect::to('user');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$validator = Validator::make(Input::all(),User::$fullRules);
		if ($validator->passes()) {
			$user = User::whereCid($id)->first();
			if (Auth::user()->cid == $user->id)
			$user->name = ucwords(strtolower(Input::get('name')));
			$user->city = ucwords(strtolower(Input::get('city')));
			$user->year = Input::get('year');
			$user->department = ucwords(strtolower(Input::get('department')));
			$user->college = ucwords(strtolower(Input::get('college')));
			$user->gender = ucfirst(Input::get('gender'));
			$user->email = strtolower(Input::get('email'));
			$user->mobile = Input::get('mobile');
			$user->save();
			return Redirect::to('user/'.$id)->with('message','Profile successfully updated!');
		}
		return Redirect::to('user/'.$id.'/edit')->with('message','Something went wrong! Correct the following errors')->withErrors($validator)->withInput();
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function image(){
		$image = Input::file('image');
		$validator = Validator::make(Input::all(),User::$imageRules); 
		if($validator->passes()) {
			$imageName = time().'c15-'.$image->getClientOriginalName();
			Image::make($image->getRealPath())
							->resize(300,300)
							->save('public/users/dp/'.$imageName);
			$user = Auth::user();
			$user->image = 'users/dp/'. $imageName;
			$user->save();

			return Redirect::back()->with('message','Image uploaded successfully!');
						
			
		}
		return Redirect::back()->with('message','Error Occured.')
						->withErrors($validator);

	}
	public function showChangePassword(){
		return View::make('users.changepassword');
		
	}

	public function confirmChangePassword(){
		
	
			$rules = array('password'=>'required|min:6|confirmed');
			$validator = Validator::make(Input::all(),$rules);
			if ($validator->passes()) {

				$user = Auth::user();
				$user->password = Hash::make(Input::get('password'));
				$user->save();
			return Redirect::to('user/'.Auth::user()->cid.'/edit')->with('message','Password changed successfully!')->with('messageType',1);
			}
			return Redirect::to('changepassword')->with('message','Your password didnt meet our expection')->with('messageType',0)
				->withErrors($validator);
		
	}

	/*public function moreinfo(){
		if(Auth::user()->college == null || Auth::user()->year == null || Auth::user()->department == null || Auth::user()->mobile == null )
		{
			if (Auth::user()->createdVia == 1) {
				return View::make('users.moreinfo')
				->with('message','Thanks. Your account Created successfully. Just provide few more details to know more about you!')->with('user',Auth::user());
			}
			else return Redirect::to('completeprofilefb')->with('message','You created the account using fb login.');
		
		}
		return Redirect::to('user/'.Auth::user()->cid)->with('message','You alredy completed the profile requirements');
	}
	public function moreinfofb(){
			if(Auth::user()->college == null || Auth::user()->year == null || Auth::user()->department == null || Auth::user()->mobile == null )
			{
				if (Auth::user()->createdVia == 0) {
				return View::make('users.moreinfoFb')
				->with('user',Auth::user());
				}
				else return Redirect::to('completeprofile');
			}
			return Redirect::to('user/'.Auth::user()->cid)->with('message','You alredy completed the profile requirements');
		}

	
public function submitMoreinfo(){

		$validator = Validator::make(Input::all(),User::$moreRules);
		if($validator->passes())
		{
			$user = User::whereCid(Auth::user()->cid)->first();
			$user->city = Input::get('city');
			$user->department = Input::get('department');
			$user->year = Input::get('year');
			$user->gender = Input::get('gender');
			$user->save();

			return Redirect::to('user/'.$user->cid)->with('message','Your details are successfully added!');
		}

		return Redirect::back()->with('message','Something went wrong!')
		->withErrors($validator)->withInput();
}
public function submitMoreinfoFb(){

		$validator = Validator::make(Input::all(),User::$moreRulesFb);
		if($validator->passes())
		{
			$user = User::whereCid(Auth::user()->cid)->first();
			$user->password = Input::get('password');
			$user->city = Input::get('city');
			$user->department = Input::get('department');
			$user->year = Input::get('year');
			$user->mobile = Input::get('mobile');
			$user->save();

			return Redirect::to('user/'.$user->cid)->with('message','Your details are successfully added!');
		}

		return Redirect::back()->with('message','Something went wrong!')
		->withErrors($validator)->withInput();
}*/
}
