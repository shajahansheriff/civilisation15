<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('users', function($table){
			$table->Bigincrements('id');
			$table->string('name');
			$table->string('cid');
			$table->string('email')->unique();
			$table->string('password');
			$table->string('college');
			$table->boolean('admin');
			$table->bigInteger('mobile');
			$table->text('remember_token');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('users');
	}

}
