<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventsTableUpdated extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events',function($table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('category_id');
			$table->foreign('category_id')->references('id')->on('eventCategories');
			$table->string('image');
			$table->text('description');
			$table->text('rules');
			$table->text('contact');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events');
	}

}
