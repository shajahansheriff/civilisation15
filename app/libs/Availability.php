<?php 

/**
* Availability Checker
*/
class Availability
{
	public static function hasDepartment($department){
		return (empty($department)) ? 'Not Available' : $department ;
		
	}
	public static function hasCollege($college){
		return (empty($college)) ? 'Not Available' : $college ;
		
	}
	public static function hasYear($year){
		return (empty($year)) ? 'Not Available' : $year ;
		
	}
	public static function hasGender($gender){
		return (empty($gender)) ? 'Not Available' : $gender ;
		
	}
	public static function hasLocation($location){
		return (empty($location)) ? 'Not Available' : $location ;
		
	}
	public static function hasEmail($email){
		return (empty($email)) ? 'Not Available' : $email ;
		
	}
	public static function hasMobile($mobile){
		if(preg_match( '/^(\d{5})(\d{5})$/', $mobile,  $matches ) )
		{
		    $mobile = '+91'.'-'.$matches[1] . '-' .$matches[2];    
		}
		return (empty($mobile)) ? 'Not Available' : $mobile ;
		
	}
}
