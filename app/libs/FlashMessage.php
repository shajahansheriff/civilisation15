<?php

class FlashMessage {
	/**
	 * get session value
	 * @param  [type] $session [true or false]
	 * @return [object]          [class and title]
	 */
	public $title;
	public $class;
	public static function get($session){
		if($session) {
			$message = array('title' =>'Oops!','class'=>'alert-danger' );
			return $message;
			
		}
		else
		{
			$message = array('title' =>'Hi-Fi!','class'=>'alert-success' );
			return $message;
		}

	}
}