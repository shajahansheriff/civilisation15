<?php

class ProfilePic {
	public static function getProfilePic($src){
		$defaultImgSrc = "https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=100";
		$src = (empty($src)) ? $defaultImgSrc : $src ;
		return $src;
	}
}