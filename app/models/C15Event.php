<?php

class C15Event extends Eloquent {

	protected $table = "events";
	public static $rules = array(
		'name' => 'required',
		'description'=> 'required',
		'contact' =>'required',
		'rules' => 'required',
		'category' => 'required'
		 );

}