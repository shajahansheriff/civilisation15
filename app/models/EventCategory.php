<?php

class EventCategory extends Eloquent {
	protected $table = 'eventCategories';
	protected $fillable = array('name');

	public static $rules = array('name'=>'required|min:2|alpha');

	/*public function events(){
		$this->hasMany('Event');
	}*/
}