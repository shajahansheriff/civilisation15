<?php

class Menu extends Eloquent {

	protected $fillable = array('name');

	public static $rules = array(
		'name'=>'required|between:2,15'
		);


}