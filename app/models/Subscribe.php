<?php

class Subscribe extends Eloquent {
	protected $table = 'subscribes';
	protected $fillable = array('user_id','event_id');

	public function user(){
		return $this->belongsTo('User');
	}
	
}