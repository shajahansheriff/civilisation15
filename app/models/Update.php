<?php

class Update extends Eloquent {
	protected $fillable = array('content');
	protected $table = 'updates';

	public static $rules = array('content' => 'required' );
}