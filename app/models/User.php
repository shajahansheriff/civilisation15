<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	protected $fillable = array('name','email','password','college','mobile','admin');
	public static $rules = array(
		'name'=>'required|min:4',
		'email'=>'required|email|unique:users',
		'password'=>'required|between:6,15',
		'college'=>'required|between:3,100',
		'mobile'=>'required|digits:10',
		);
	public static $fullRules = array(
		'name'=>'required|min:4',
		'email'=>'required|email',
		'college'=>'required|between:3,100',
		'mobile'=>'required|digits:10',
		'gender'=>'required',
		'city'=>'required|min:3',
		'department'=>'required|min:2',
		'year'=>'required'
		);
	public static $moreRules = array(
		'department'=>'required|min:2',
		'year'=>'required',
		'city'=>'required|min:3',
		'gender'=>'required'
		);
	public static $moreRulesFb = array(
		'password'=>'required|min:6',
		'college'=>'required|min:3',
		'department'=>'required|min:2',
		'year'=>'required',
		'city'=>'required|between:3,100',
		'mobile'=>'required|digits:10'
		);
	public static $imageRules = array(
		'image'=> 'image|required'
		);

	public function getAuthPassword(){
		return $this->password;
	}

	public function getRememberToken()
	{
	    return $this->remember_token;
	}

	public function setRememberToken($value)
	{
	    $this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
	    return 'remember_token';
	}

	public function profiles(){
		return $this->hasMany('Profile');
	}

	public function subscribes(){
		return $this->hasMany('Subscribe');
	}
}
