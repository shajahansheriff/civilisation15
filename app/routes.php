<?php

Route::get('/', array('as'=>'root','uses'=>'OtherController@root'));

/**
 * User routes
 */
Route::get('signin',array('as'=>'getUserSingin', 'uses'=>'SignInController@index'));
Route::post('signin',array('as'=>'userSignin','uses'=>'SignInController@login'));
Route::get('changepassword',array('uses'=>'UserController@showChangePassword'));
Route::post('changepassword',array('uses'=>'UserController@confirmChangePassword'));
Route::resource('user','UserController');
Route::post('/user/image','UserController@image');
Route::get('completeprofile','CompleteProfileController@show');
Route::post('completeprofile','CompleteProfileController@submit');
Route::post('completeprofilefb','CompleteProfileController@submitFb');

//fb

Route::get('login/fb',array('as'=>'fbLogin','uses'=>'FacebookController@getLogin'));
Route::get('login/fb/callback', array('as'=>'fbLoginCallback','uses'=>'FacebookController@getLoginCallback'));

/**
 * -/User Routes
 */

/**
 * Subscribe routes
 */
Route::resource('subscribe', 'SubscribeController');
// end of subscribe routes

Route::get('sponsors',array('as'=>'sponsors','uses'=>'OtherController@sponsors'));

/*Route::get('events',function(){
	return View::make('events');
});*/

Route::resource('events','EventController');

Route::get('c14', array('as'=>'c14_view','uses'=>'OtherController@c14'));
Route::get('contact',function(){
	return View::make('others.contact');
});
Route::get('about', array('as'=>'about','uses'=>'OtherController@about'));
Route::get('workshops',array('as'=>'workshops','uses'=>'OtherController@workshops'));
Route::get('accomodation',array('as'=>'accomodation','uses'=>'OtherController@accomodation'));
Route::get('lectures',array('as'=>'lectures','uses'=>'OtherController@lectures'));


/**
 * Admin Routes
 */
Route::group(['prefix'=>'c15/admin'],function(){
	Route::resource('events','AdminEventController');
	Route::resource('eventCategory','AdminEventCategoryController');
	Route::resource('updatecenter','AdminUpdateController');
	Route::resource('eventeditor','EventEditorController');
});

Route::resource('/menus','MenuController');
Route::get('c15/admin/contact',array('uses'=>'HomeController@showWelcome'));
Route::controller('c15/admin','AdminController');

/**
 * End of ADmin Routes
 */

Route::get('logout',function(){
	Auth::logout();
	return Redirect::to('/');
});