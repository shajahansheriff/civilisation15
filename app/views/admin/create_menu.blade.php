@extends('admin.layouts.main')
@section('content')
<div class="row" ng-controller="menuCtrl">
	<!-- left column -->
	<div class="col-md-6">
		<!-- general form elements disabled -->
		<div class="box box-warning">
			<div class="box-header">
				<h3 class="box-title">Main Menu List</h3>
				<div class="pull-right box-tools">
									<button class="btn btn-danger btn-sm refresh-btn" data-toggle="tooltip" title="Reload"><i class="fa fa-refresh"></i></button>
									<button class="btn btn-danger btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
									<button class="btn btn-danger btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
								</div>
				</div><!-- /.box-header -->
				<div class="box-body">
					<ul>
						<div class="comment" ng-hide="loading"> </div>
						<li ng-repeat="menu in menus"> <% menu.name %><a href="#" ng-click="deleteMenu(menu.id)" class="text-muted">Delete</a> </li>
					</ul>	
				</div><!-- /.box-body -->
					</div><!-- /.box -->
					</div><!--/.col (right) -->
					<div class="col-md-6">
						<div class="box box-primary">
							<div class="box-header">
								<h3 class="box-title">Quick Example</h3>
								<div class="pull-right box-tools">
									<button class="btn btn-danger btn-sm refresh-btn" data-toggle="tooltip" title="Reload"><i class="fa fa-refresh"></i></button>
									<button class="btn btn-danger btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
									<button class="btn btn-danger btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
								</div>
								</div><!-- /.box-header -->
								<!-- form start -->
								<p class="text-center" ng-show="loading"><span class="fa fa-meh-o fa-5x fa-spin"></span></p>
								<form role="form"  ng-submit="submitMenu()">
								
									<div class="box-body">
										<div class="form-group">
											<div class="alert alert-warning alert-dismissable">
		                                        <i class="fa fa-warning"></i>
		                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                                        <b>Alert!</b> <% menuError %>
                                    		</div>
											<label for="exampleInputEmail1">Menu Name:</label>
											<input type="text"  ng-model="menuData.name" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter Menu Name">
										</div>
										
										</div><!-- /.box-body -->
										<div class="box-footer">
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
									</form>
									</div><!-- /.box -->
								</div>
							</div>
							@stop