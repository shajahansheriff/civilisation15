@extends('admin.layouts.main')

@section('content')
<div class="row">
	<div class="col-md-4">
		<h3>Create Category</h3>
		<div class="form-group">
		<input type="text" value="" class="form-control" id="categoryName" name="categoryName" placeholder="Eg: Online">
		</div>
		<button class="btn btn-primary" id="createCategory" class="">Create Category</button>
	</div>
	<div class="col-md-5">
		<h3>List of Category</h3>
		
		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Name</th>
					</tr>
				</thead>
				<tbody>
				@foreach ($categories as $category)
		 			<tr>
						<td>{{ $category->name }}</td>
						<td><button><span class="fa fa-edit text-primary"></span></button></td>
						<td><button class="deleteCategory" id="{{ $category->id }}"><span class="fa fa-close text-danger"></span></button></td>
					</tr>
				@endforeach


				</tbody>
			</table>
		</div>
	</div>
</div>

@stop
