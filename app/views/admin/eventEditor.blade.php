@extends('admin.layouts.main')

@section('content')
<div class="form col-md-6">
<div class="form-group">
	{{ Form::select('category', array('0' => 'Engineering', '1'=>'Funda','2'=>'Online'),null,array('class'=>'form-control'));}}
</div>
<div class="form-group">
<label for="eventName">Event Name:</label>
<input type="text" name="eventName" id="eventName" class="form-control" value="" required="required">
</div>
</div>

<div class="form col-md-6">
<div class="col-md-12 form-group">
<label for="descEditor">Event Description</label>
<textarea id="descEditor" class="form-control col-md-10"></textarea>
</div>
</div>


<div class="form col-md-6">
<div class="col-md-12 form-group">
<label for="rulesEditor">Event Rules</label>
<textarea id="rulesEditor" class="form-control col-md-10"></textarea>
</div>

</div>

<div class="form col-md-6">
<div class="col-md-12 form-group">
<label for="contactEditor">Event Contact</label>
<textarea id="contactEditor" class="form-control col-md-10"></textarea>
</div>

</div>
<div class="col-md-12"> 
<div class="alert alert-info">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<strong>Congrats!</strong> Event successfully created. Refresh the page to create the Next Event.
</div>
<button type="button" class="btn btn-block btn-primary" id="saveEvent">Click Here to Save the Event</button>
<br>
<br>
<br>
</div>

@stop
