<!DOCTYPE html>
<html ng-app="adminApp">
    <head>
        <meta charset="UTF-8">
        <title>Admin Panel | c15</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        
        {{ HTML::style('bower/bootstrap/dist/css/bootstrap.min.css') }}
        <!-- font Awesome -->
        
        {{ HTML::style('bower/font-awesome/css/font-awesome.min.css') }}
        <!-- Ionicons -->
        
        {{ HTML::style('assets/css/admin/ionicons.min.css') }}
        <!-- Morris chart -->
        
        {{ HTML::style('assets/css/admin/morris/morris.css') }}
        <!-- jvectormap -->
        
        {{ HTML::style('assets/css/admin/jvectormap/jquery-jvectormap-1.2.2.css') }}
        <!-- fullCalendar -->
        
        {{ HTML::style('assets/css/admin/fullcalendar/fullcalendar.css') }}
        <!-- Daterange picker -->
        
        {{ HTML::style('assets/css/admin/daterangepicker/daterangepicker-bs3.css') }}
        <!-- bootstrap wysihtml5 - text editor -->
        
        {{ HTML::style('bower/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.min.css') }}
        <!-- Theme style -->
        
        {{ HTML::style('assets/css/admin/AdminLTE.css') }}
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue" >
        <!-- header logo: style can be found in header.less -->
        @if(Auth::check())
            <header class="header">
                <a href="admin" class="logo">
                    <!-- Add the class icon to your logo image or logo icon to add the margining -->
                    C-15 Admin Panel
                </a>
             @include('admin.layouts.navbar')
            </header>

            @include('admin.layouts.sidebar')
        @endif
         <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard <% name %>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
     <!-- Main content -->
    <section class="content">

        @yield('content')


    </section><!-- /.content -->
    </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->
        
        {{ HTML::script('js/globaldepend.min.js') }}
        {{ HTML::script('bower/jquery/dist/jquery.min.js') }}
        {{ HTML::script('bower/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.js') }}
        {{ HTML::script('js/adminFrontend.js') }}
        {{ HTML::script('assets/js/jquery/eventsRouter.js') }}
        {{ HTML::script('assets/js/jquery/adminCrud.js') }}
        {{ HTML::script('assets/js/jquery/eventEditor.js') }}

    </body>
</html>