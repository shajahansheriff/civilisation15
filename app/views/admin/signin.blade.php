@extends('admin.layouts.main')
@section('content')
<div class="form-box" id="login-box">
@if (Session::has('message'))
		
			<div class="alert alert-block alert-error fade in">
				<button type="button" class="close" data-dismiss="alert">×</button>
				{{ Session::get('message') }}
	 		</div>
		
@endif

		@if ($errors->has()) 
		<ul>
			@foreach ($errors->all() as $error) 
			<li>
				{{$error}}
			</li>
			@endforeach
		</ul>
		@endif
	<div class="header">Sign In</div>
	{{ Form::open(array('url'=>'c15/admin/signin','method'=>'post')) }}
		<div class="body bg-gray">
			<div class="form-group">
				{{ Form::text('email',null,array('class'=>'form-control','placeholder'=>'Email')) }}	
			</div>
			<div class="form-group">
				<input type="password" name="password" placeholder="Password" class="form-control">
			</div>

			<!-- <div class="form-group">
				<input type="checkbox" name="remember_me"/> Remember me
			</div> -->
		</div>
		<div class="footer">
			
			{{ Form::submit('Sign In',array('class'=>'btn bg-olive btn-block')) }}

			<p><a href="#">I forgot my password</a></p>
			<!-- {{ HTML::link('c15/admin/signup','Register a new membership') }} -->
		</div>
		{{ Form::close() }}
	<!-- <div class="margin text-center">
		<span>Sign in using social networks</span>
		<br/>
		<button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
		<button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button>
		<button class="btn bg-red btn-circle"><i class="fa fa-google-plus"></i></button>
	</div> -->
</div>
@stop