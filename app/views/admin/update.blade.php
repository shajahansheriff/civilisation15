@extends('admin.layouts.main')

@section('content')
<div class="row">
	<div class="col-md-4">
		<h3>Create Update</h3>
		<div class="form-group">
		<textarea id="updateContent" class="form-control" placeholder="Update" ></textarea>
		</div>

		<button class="btn btn-primary" id="createCategory" class="">Create Category</button>
	</div>
	<div class="col-md-5">
		<h3>List of Category</h3>
		
		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Name</th>
					</tr>
				</thead>
				<tbody>
				@if($updates) 
				@foreach ($updates as $update)
		 			<tr>
						<td>{{ $update->content }}</td>
						<td><button><span class="fa fa-edit text-primary"></span></button></td>
						<td><button class="deleteCategory" id="{{ $update->id }}"><span class="fa fa-close text-danger"></span></button></td>
					</tr>
				@endforeach
				@endif


				</tbody>
			</table>
		</div>
	</div>
</div>

@stop
