@extends('layouts.main')
@section('content')
<section class="event sky">
  <div class="container">
  <div id="eventLoading">
    <div class="tp-loader spinner"></div>
  </div>
    <div class="row">
      <div class="col-sm-3 col-md-3">
        <div class="panel-group" id="accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="fa fa-gears">
              </span>&nbsp Engineering</a>
              </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
              <ul class="list-group">
                @foreach($events as $event)
                @if($event->category_id==0)
                <li class="list-group-item" id="{{ $event->id }}"><a href="#">{{ $event->name }}</a></li>
                @endif
                @endforeach
                <!-- <li class="list-group-item"><a href="#" id="event1">Event 1</a></li>
                <li class="list-group-item"><a href="#">Event 2</a></li>
                <li class="list-group-item"> <a href="#">Event 3</a></li> -->
              </ul>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-heart ">
              </span>Funda</a>
              </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
              
              <ul class="list-group">
                @foreach($events as $event)
                @if($event->category_id==1)
                <li class="list-group-item" id="{{ $event->id }}"><a href="#">{{ $event->name }}</a></li>
                @endif
                @endforeach
                <!-- <li class="list-group-item"><a href="#" id="event1">Event 1</a></li>
                <li class="list-group-item"><a href="#">Event 2</a></li>
                <li class="list-group-item"> <a href="#">Event 3</a></li> -->
              </ul>
              <!-- <div class="list-group">
                <a href="#" class="list-group-item">
                  Cras justo odio
                </a>
                <div class="list-group">
                  <a href="#" class="list-group-item">
                    Cras justo odio
                  </a>
                  <a href="#" class="list-group-item">Dapibus ac facilisis in</a>
                  <a href="#" class="list-group-item">Morbi leo risus</a>
                  <a href="#" class="list-group-item">Porta ac consectetur ac</a>
                  <a href="#" class="list-group-item">Vestibulum at eros</a>
                </div>
                <a href="#" class="list-group-item">Dapibus ac facilisis in</a>
                <a href="#" class="list-group-item">Morbi leo risus</a>
                <a href="#" class="list-group-item">Porta ac consectetur ac</a>
                <a href="#" class="list-group-item">Vestibulum at eros</a>
              </div> -->
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><span class="fa fa-globe">
              </span>&nbsp Online</a>
              </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
              <ul class="list-group">
                @foreach($events as $event)
                @if($event->category_id==2)
                <li class="list-group-item" id="{{ $event->id }}"><a href="#">{{ $event->name }}</a></li>
                @endif
                @endforeach
                <!-- <li class="list-group-item"><a href="#" id="event1">Event 1</a></li>
                <li class="list-group-item"><a href="#">Event 2</a></li>
                <li class="list-group-item"> <a href="#">Event 3</a></li> -->
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-9 col-md-9 col-xs-12 events-panel">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#description" data-toggle="tab" id="descTab">Description</a></li>
                <li><a href="#rules" data-toggle="tab">Rules</a></li>
                <li><a href="#contact" data-toggle="tab">Contact</a></li>
              </ul>
            </div>
              <div class="tab-content" style="padding:0; padding-top:15px;">
                
                <!-- description -->
                <div class="tab-pane active" id="description">
                  Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.

                </div><!-- /#description -->
                  
                <!-- rules-->
                <div class="tab-pane" id="rules">
                  Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.2
                </div><!-- /#rules -->
                <div class="tab-pane" id="contact">
                    Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.3
                </div><!-- /#contact -->
                <button  status="0" stage="{{ (!Auth::check()) ? '0' : '1' }}" id="subscribe" class="btn btn-primary tooltip-right" title=" {{ (!Auth::check()) ?'SignIn to subscribe' : 'To Get updates! about this event'}} " style="padding:2px 8px 2px; margin-top:10px;" data-toggle="{{ (!Auth::check()) ? 'modal' : '' }}" href="#account" event="" >Subscribe</button>
                <h4>
                </h4>
              </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div>
          </div>
        </div>
    </div>

</section>
@stop