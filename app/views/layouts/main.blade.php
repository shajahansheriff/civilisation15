<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Civilization'15">
    <meta name="author" content="CodeBreaker">
    <title>Civilization'15 | Welcome </title>
	
	<!-- core CSS -->
       <!-- bootstrap 3.0.2 -->
<style type="text/css">
    
.tp-loader  {   
        top:50%; left:50%; 
        z-index:10000; 
        position:absolute;
        

      }
      
.tp-loader.spinner {
  width: 40px;
  height: 40px;
  background:url(http://i.imgur.com/ICOsAmq.gif) no-repeat center center;
  background-color: #fff;
  box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.15);
  -webkit-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.15);  
  margin-top:-20px;
  margin-left:-20px;
  -webkit-animation: tp-rotateplane 1.2s infinite ease-in-out;
  animation: tp-rotateplane 1.2s infinite ease-in-out;
  border-radius: 3px;
  -moz-border-radius: 3px;
  -webkit-border-radius: 3px;
}



@-webkit-keyframes tp-rotateplane {
  0% { -webkit-transform: perspective(120px) }
  50% { -webkit-transform: perspective(120px) rotateY(180deg) }
  100% { -webkit-transform: perspective(120px) rotateY(180deg)  rotateX(180deg) }
}

@keyframes tp-rotateplane {
  0% { 
    transform: perspective(120px) rotateX(0deg) rotateY(0deg);
    -webkit-transform: perspective(120px) rotateX(0deg) rotateY(0deg) 
  } 50% { 
    transform: perspective(120px) rotateX(-180.1deg) rotateY(0deg);
    -webkit-transform: perspective(120px) rotateX(-180.1deg) rotateY(0deg) 
  } 100% { 
    transform: perspective(120px) rotateX(-180deg) rotateY(-179.9deg);
    -webkit-transform: perspective(120px) rotateX(-180deg) rotateY(-179.9deg);
  }
}

</style>
        {{ HTML::style('bower/bootstrap/dist/css/bootstrap.min.css') }}
        {{ HTML::style('assets/css/jasny-bootstrap.min.css') }}
        <!-- font Awesome -->
        
        {{ HTML::style('bower/font-awesome/css/font-awesome.min.css') }}
        {{ HTML::style('assets/css/animate.min.css') }}
        {{ HTML::style('assets/css/prettyPhoto.css') }}
        {{ HTML::style('assets/css/main.css') }}
        {{ HTML::style('assets/css/responsive.css') }}
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="{{ URL::asset('assets/images/favicon.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png"> 
</head><!--/head-->

<body class="homepage">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-58137619-1', 'auto');
  ga('send', 'pageview');

</script>
<div class="tp-loader spinner" id="mainLoading"></div>
<header id="header">
        

        <nav class="navbar navbar-inverse" role="banner">

            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{URL::to('/')}}">{{ HTML::image('assets/images/logo.png','Civilisation 15',array('width'=>'220','height'=>'20','class'=>'img-responsive')) }}</a>
                   
                </div>
                
                <div class="collapse navbar-collapse navbar-right">

                    <ul class="nav navbar-nav">
                        <li class="(Route::currentRouteName()=='root')">{{ HTML::link('/','Home') }}</li>
                         <!-- <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Events <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li>{{ HTML::link('/','Online Events') }}</li>
                                <li>{{ HTML::link('/','Funda Events') }}</li>
                                <li>{{ HTML::link('/','Enginnering Events') }}</li>
                            </ul>
                        </li>  -->
                        <li class="{{ (Route::currentRouteName()=='events.index') ? 'active' : ''}} ">{{ HTML::link('events','Events') }}</li>
                        <li class="{{ (Route::currentRouteName()=='workshops') ? 'active' : ''}} " >{{ HTML::link('workshops','Workshops') }}</li>
                        <li class="{{ (Route::currentRouteName()=='lectures') ? 'active' : ''}} ">{{ HTML::link('lectures','Lectures') }}</li> 
                        <li class="{{ (Route::currentRouteName()=='accomodation') ? 'active' : ''}} ">{{ HTML::link('accomodation','Accomodation') }}</li>
                        @if(!Auth::check())
                        <li><a data-toggle="modal" href="#account">SignIn/Register</a></li>
                        @else 
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <?php $username = explode(' ', Auth::user()->name); ?>
                            Hi, {{ ucfirst($username[0]) }} <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <?php $id = Auth::user()->cid; ?>
                                <li><a href="{{ URL::to('user/'.$id)}}">Profile</a></li>
                                <li>{{ HTML::link('logout','Logout') }}</li>
                            </ul>
                        </li>
                        @endif 
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="top-number"><p><i class="fa fa-phone-square"></i> <b>Updates: </b>Will be updated soon!!  </p></div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                       <div class="social">
                            <ul class="social-share">
                                <li><a href="http://www.facebook.com/civilisationceg" target="_new"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="http://www.twitter.com/civilisationceg" target="_new"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                       </div>
                    </div>
                </div>
            </div><!--/.container-->
        </div><!--/.top-bar-->
    </header><!--/header-->

    @yield('content')

<div class="modal fade" id="account">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Sign In<small> to Civilisation</small></h4>
            </div>
            <div class="modal-body">
                {{ Form::open(array('url'=>'signin')) }}
            <!-- <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input type="text" class="form-control" placeholder="Name" tabindex="1">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-key"></i></span>
                        <input type="password" name="email" id="email" class="form-control " placeholder="Password" tabindex="2">
                    </div>
                </div>
            </div> -->
            <br>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="email" name="email" id="email" required class="form-control" placeholder="email" tabindex="2">
            </div> 
            <br>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-paw"></i></span>
                <input type="password" name="password" id="password" required class="form-control" placeholder="Password" tabindex="3">
            </div>   
            <div class="checkbox">
              <label>
                  <input type="checkbox" value="true" name="remember" id="remember"> Remember login
              </label>
            </div>
            <!-- <div class="form-group row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input type="password" class="form-control" placeholder="Password" tabindex="1">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-key"></i></span>
                        <input type="password" name="confirmPassword" id="confirmPassword" class="form-control " placeholder="Confirm Password" tabindex="2">
                    </div>
                </div>
            </div> -->
            <div class="row">
                <div class="col-xs-12 col-md-6"><input type="submit" value="Sign In" class="btn btn-success btn-block " tabindex="7"></div> <div class="col-md-1 col-xs-12" style="padding-top:7px; text-align:center;"><b >Or</b></div> 
                <div class="col-xs-12 col-md-5"><a href="{{ URL::to('login/fb') }}" class="btn btn-info btn-block"><span class="fa fa-facebook-square"></span> &nbsp Login with Facebook</a></div>
            </div>
            
            <div class="row">
                
            </div>
        {{ Form::close() }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="{{ URL::to('user/create') }}" type="button" class="btn btn-primary">Create a new Account</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
    <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2015 <a target="_blank" href="http://www.facebook.com/civilisationceg">Civilisation CEG</a>. All Rights Reserved.
                </div>
                <div class="col-sm-6">
                    <ul class="pull-right">
                      
                        <li><div class="fb-like" data-href="https://www.facebook.com/CivilisationCEG" data-width="150" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div></li>
                        <li>{{ HTML::link('/','Home') }}</li>
                        <li>{{ HTML::link('c14','C\'14') }}</li>
                        <li>{{ HTML::link('about','About Us') }}</li>
                        <li>{{ HTML::link('sponsors','Sponsors') }}</li>
                        <li>{{ HTML::link('contact','Contact') }}</li>
                    </ul>
                    
                </div>
            </div>
        </div>
    </footer><!--/#footer-->
    {{ HTML::script('js/userglobalDepend.js') }}
    {{ HTML::script('assets/js/jasny-bootstrap.min.js') }}
    @if(Route::currentRouteName()=='user.edit')
    {{ HTML::script('assets/js/user.js') }}
    @endif
    @if(Route::currentRouteName()=='events.index')
    {{ HTML::script('assets/js/jquery/eventsRouter.js') }}
    @endif
    @if(Route::currentRouteName() == 'c14_view')
    {{ HTML::script('assets/js/jquery.prettyPhoto.js') }}
    {{ HTML::script('assets/js/jquery.isotope.min.js') }}
    {{ HTML::script('assets/js/main.js') }}
    {{ HTML::script('assets/js/wow.min.js') }}
    @endif
    <div id="fb-root"></div>
    <script type="text/javascript">
        $(window).load(function() {
        $('#mainLoading').hide('fast');
        });
    </script>
    <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=376328769207026&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

<script type="text/javascript">
    if (window.location.hash && window.location.hash == '#_=_') {
        window.location.hash = '';
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
  $('.tooltip-right').tooltip({
    placement: 'right',
    viewport: {selector: 'body', padding: 2}
  })
});
</script>
</body>
</html>