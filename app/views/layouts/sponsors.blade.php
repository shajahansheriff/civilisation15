@extends('layouts.main')
@section('content')

<section id="partner">
        <div class="container sponsors">
            <div class="center wow fadeInDown animated" style="visibility: visible; -webkit-animation: fadeInDown;">
                <h2>Our Sponsors</h2>
                
                 {{ HTML::image('assets/images/sponsorspage.jpg','sponsors',array('class'=>'img-responsive')) }}
            </div>    

           <!--  <div class="partners">
                <ul>
                    
                    <li> <a href="#"> 
                        {{ HTML::image('assets/images/sponsors/1.jpg','Sponcer 1',array('stlye'=>'visibility: visible; -webkit-animation: fadeInDown 1000ms 300ms;','data-wow-delay'=>'300ms','data-wow-duration'=>'1000ms','class'=>'img-responsive wow fadeInDown animated')) }} </a>
                    </li>
                    <li> <a href="#"> 
                        {{ HTML::image('assets/images/sponsors/2.jpg','Sponcer 1',array('stlye'=>'visibility: visible; -webkit-animation: fadeInDown 1000ms 300ms;','data-wow-delay'=>'300ms','data-wow-duration'=>'1000ms','class'=>'img-responsive wow fadeInDown animated')) }} </a>
                    </li>
                    <li> <a href="#"> 
                        {{ HTML::image('assets/images/sponsors/3.jpg','Sponcer 1',array('stlye'=>'visibility: visible; -webkit-animation: fadeInDown 1000ms 300ms;','data-wow-delay'=>'300ms','data-wow-duration'=>'1000ms','class'=>'img-responsive wow fadeInDown animated')) }} </a>
                    </li>
                    <li> <a href="#"> 
                        {{ HTML::image('assets/images/sponsors/4.jpg','Sponcer 1',array('stlye'=>'visibility: visible; -webkit-animation: fadeInDown 1000ms 300ms;','data-wow-delay'=>'300ms','data-wow-duration'=>'1000ms','class'=>'img-responsive wow fadeInDown animated')) }} </a>
                    </li>
                    
                    <li> <a href="#"> 
                        {{ HTML::image('assets/images/sponsors/5.jpg','Sponcer 1',array('stlye'=>'visibility: visible; -webkit-animation: fadeInDown 1000ms 300ms;','data-wow-delay'=>'300ms','data-wow-duration'=>'1000ms','class'=>'img-responsive wow fadeInDown animated')) }} </a>
                    </li>
                    
                    <li> <a href="#"> 
                        {{ HTML::image('assets/images/sponsors/6.jpg','Sponcer 1',array('stlye'=>'visibility: visible; -webkit-animation: fadeInDown 1000ms 300ms;','data-wow-delay'=>'300ms','data-wow-duration'=>'1000ms','class'=>'img-responsive wow fadeInDown animated')) }} </a>
                    </li>
                    
                    <li> <a href="#"> 
                        {{ HTML::image('assets/images/sponsors/7.jpg','Sponcer 1',array('stlye'=>'visibility: visible; -webkit-animation: fadeInDown 1000ms 300ms;','data-wow-delay'=>'300ms','data-wow-duration'=>'1000ms','class'=>'img-responsive wow fadeInDown animated')) }} </a>
                    </li>
                    
                    <li> <a href="#"> 
                        {{ HTML::image('assets/images/sponsors/8.jpg','Sponcer 1',array('stlye'=>'visibility: visible; -webkit-animation: fadeInDown 1000ms 300ms;','data-wow-delay'=>'300ms','data-wow-duration'=>'1000ms','class'=>'img-responsive wow fadeInDown animated')) }} </a>
                    </li>
                    
                    <li> <a href="#"> 
                        {{ HTML::image('assets/images/sponsors/9.jpg','Sponcer 1',array('stlye'=>'visibility: visible; -webkit-animation: fadeInDown 1000ms 300ms;','data-wow-delay'=>'300ms','data-wow-duration'=>'1000ms','class'=>'img-responsive wow fadeInDown animated')) }} </a>
                    </li>
                    
                    <li> <a href="#"> 
                        {{ HTML::image('assets/images/sponsors/10.jpg','Sponcer 1',array('stlye'=>'visibility: visible; -webkit-animation: fadeInDown 1000ms 300ms;','data-wow-delay'=>'300ms','data-wow-duration'=>'1000ms','class'=>'img-responsive wow fadeInDown animated')) }} </a>
                    </li>
                    
                </ul>

            </div> -->

        </div><!--/.container-->
    </section>

@stop