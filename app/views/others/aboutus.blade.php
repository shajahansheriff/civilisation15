@extends('layouts.main')
@section('content')
<section id="portfolio">
	<div class="container">
		<div class="center">
			<h2>About CEG</h2>
			<p class="lead">Starting out in 1794 as a Survey School, it became the first Civil Engineering institute in the country, which later extended to other engineering disciplines. Preserving its rich heritage, it remains consistent with top excellent faculty and top notch research. Its eminence to elicit the most influential and reputed personalities in different spheres is outstanding.</p>
		</div>
		<div class="center">
			<h2>Society of Civil Engineers</h2>
			<p class="lead">The Society of Civil Engineers (SCE) is a Student organization, guided by the department of Civil Engineering which provides space for the students to develop their technical knowledge in the field of Civil Engineering. SCE is also known to act as a bridge between theoretical knowledge and practical applications in the field of Civil Engineering. Students are also encouraged to put forth their innovative ideas and enhance their technical knowledge through sessions conducted on a weekly basis.
			</p>
			<p class="lead">
 With an untiring motive of ‘Conquering Horizons’, SCE presents the students of Civil Engineering in the college with a platform to showcase their talents in their expertise. Civilisation is National Level Technical Syposium on Civil Engineering, acting as a forum for all Civil Engineering students across the nation, to portray their skills and get them rewarded. Civilisation’15, 32nd edition, upholds the tagline that goes as ‘Concept to Creation’, which encourages students to to roll out their concepts as real creations to serve the humanity.</p>
		</div>
	</div>
</section>
@stop