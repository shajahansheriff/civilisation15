@extends('layouts.main')

@section('content')

<section id="portfolio">
        <div class="container">
            <div class="center">
               <h2>Civilisation'14 Gallery</h2>
                <p class="lead">Civilisation’14, 31st edition was conducted between 27th – 29th March, 2014, with tagline “Come realize, Get Civilised”. Indeed, C’14 was a great success and thanks to the students who participated and our sponsors, without them this would have been difficult.</p> 
            </div>
        
	<!-- 
	            <ul class="portfolio-filter text-center">
	                <li><a class="btn btn-default active" href="#" data-filter="*">All Works</a></li>
	                <li><a class="btn btn-default" href="#" data-filter=".bootstrap">Creative</a></li>
	                <li><a class="btn btn-default" href="#" data-filter=".html">Photography</a></li>
	                <li><a class="btn btn-default" href="#" data-filter=".wordpress">Web Development</a></li>
	            </ul> --><!--/#portfolio-filter-->

            <div class="row">
                <div class="portfolio-items">
                    <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src=" {{ URL::asset('assets/images/c14/sm/1.jpg') }}" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <a class="preview" href="{{ URL::asset('assets/images/c14/1.jpg') }}" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
                    <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src=" {{ URL::asset('assets/images/c14/sm/2.jpg') }}" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <a class="preview" href="{{ URL::asset('assets/images/c14/2.jpg') }}" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
                    <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src=" {{ URL::asset('assets/images/c14/sm/3.jpg') }}" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <a class="preview" href="{{ URL::asset('assets/images/c14/3.jpg') }}" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
                    <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src=" {{ URL::asset('assets/images/c14/sm/4.jpg') }}" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <a class="preview" href="{{ URL::asset('assets/images/c14/4.jpg') }}" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
                    <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src=" {{ URL::asset('assets/images/c14/sm/5.jpg') }}" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <a class="preview" href="{{ URL::asset('assets/images/c14/5.jpg') }}" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
                    <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src=" {{ URL::asset('assets/images/c14/sm/6.jpg') }}" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <a class="preview" href="{{ URL::asset('assets/images/c14/6.jpg') }}" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
                    <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src=" {{ URL::asset('assets/images/c14/sm/7.jpg') }}" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <a class="preview" href="{{ URL::asset('assets/images/c14/7.jpg') }}" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
                    <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src=" {{ URL::asset('assets/images/c14/sm/8.jpg') }}" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <a class="preview" href="{{ URL::asset('assets/images/c14/8.jpg') }}" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
                    <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src=" {{ URL::asset('assets/images/c14/sm/9.jpg') }}" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <a class="preview" href="{{ URL::asset('assets/images/c14/9.jpg') }}" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
                    <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src=" {{ URL::asset('assets/images/c14/sm/10.jpg') }}" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <a class="preview" href="{{ URL::asset('assets/images/c14/10.jpg') }}" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
                    <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src=" {{ URL::asset('assets/images/c14/sm/11.jpg') }}" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <a class="preview" href="{{ URL::asset('assets/images/c14/11.jpg') }}" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
                    <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src=" {{ URL::asset('assets/images/c14/sm/12.jpg') }}" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <a class="preview" href="{{ URL::asset('assets/images/c14/12.jpg') }}" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
                    <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src=" {{ URL::asset('assets/images/c14/sm/13.jpg') }}" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <a class="preview" href="{{ URL::asset('assets/images/c14/13.jpg') }}" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
                    <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src=" {{ URL::asset('assets/images/c14/sm/14.jpg') }}" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <a class="preview" href="{{ URL::asset('assets/images/c14/14.jpg') }}" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
                    <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src=" {{ URL::asset('assets/images/c14/sm/15.jpg') }}" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <a class="preview" href="{{ URL::asset('assets/images/c14/15.jpg') }}" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
                    <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src=" {{ URL::asset('assets/images/c14/sm/16.jpg') }}" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <a class="preview" href="{{ URL::asset('assets/images/c14/16.jpg') }}" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
                </div>
            </div>
        </div>
    </section><!--/#portfolio-item-->

@stop