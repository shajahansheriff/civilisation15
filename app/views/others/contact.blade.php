	@extends('layouts.main')

	@section('content')

	<section id="services" class="service-item">
		   <div class="container">
	            <div class="center wow fadeInDown">
	                <h2>Our Team</h2>
	                <!-- <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p> -->
	            </div>

	            <div class="row">

	                <div class="col-sm-6 col-md-4">
	                    <div class="media services-wrap wow fadeInDown">
	                        <div class="pull-left">
								{{ HTML::image('assets/images/services/services5.png','event1',array('class'=>'img-responsive')) }}							
	                        </div>
	                        <div class="media-body">
	                            <h3 class="media-heading">General Contacts</h3>
	                            <p>Sabarish T</p>
	                            <p><i class="fa fa-phone-square"></i> +91-9659259356 <br>
	                            <p>Jeevitha </p>
	                            <p><i class="fa fa-phone-square"></i> +91-8508077723 <br>
	                            <!-- <i class="fa fa-envelope"></i> qwffhzdhzdfh@gmail.com</p> -->
	                        </div>
	                    </div>
	                </div>
	                <div class="col-sm-6 col-md-4">
	                    <div class="media services-wrap wow fadeInDown">
	                        <div class="pull-left">
								{{ HTML::image('assets/images/services/services2.png','event1',array('class'=>'img-responsive')) }}							
	                        </div>
	                        <div class="media-body">
	                            <h3 class="media-heading">Events</h3>
	                            <small class="text-muted">(events @civilisationceg.com)</small>
	                            <p>Althaaf M</p>
	                            <p><i class="fa fa-phone-square"></i> +91-9500439245 <br>
	                            <p>Padma Priya</p>
	                            <p><i class="fa fa-phone-square"></i> +91-8903471902 <br>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-sm-6 col-md-4">
	                    <div class="media services-wrap wow fadeInDown">
	                        <div class="pull-left">
								{{ HTML::image('assets/images/services/services3.png','event1',array('class'=>'img-responsive')) }}							
	                        </div>
	                        <div class="media-body">
	                            <h3 class="media-heading">Workshop</h3>
	                            <small class="text-muted">(workshops @civilisationceg.com)</small>
	                            <p>Vignesh</p>
	                            <p><i class="fa fa-phone-square"></i> +91-9944447046 <br>
	                            <p>Kiran Bedi</p>
	                            <p><i class="fa fa-phone-square"></i> +91-9789791083 <br>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-sm-6 col-md-4">
	                    <div class="media services-wrap wow fadeInDown">
	                        <div class="pull-left">
								{{ HTML::image('assets/images/services/services1.png','event1',array('class'=>'img-responsive')) }}							
	                        </div>
	                        <div class="media-body">
	                            <h3 class="media-heading">Industry Relations</h3>
	                            <small class="text-muted">(industryrelations @civilisationceg.com)</small>
	                            <p>Peer Jaffer Sathick</p>
	                            <p><i class="fa fa-phone-square"></i> +91-9940235638 <br>
	                            <p>Pravitha Sharon</p>
	                            <p><i class="fa fa-phone-square"></i> +91-9585510349 <br>
	                            <p>Sidhu</p>
	                            <p><i class="fa fa-phone-square"></i> +91-9677447916 <br>
	                        </div>
	                    </div>
	                </div>
                                               
	            </div><!--/.row-->
	        </div><!--/.container-->
	    </section><!--/#services-->

	@stop