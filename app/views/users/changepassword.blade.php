@extends('layouts.main')

@section('content')
<section>
	<div class="container">
	@if(Session::has('message'))
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Oops!</strong> {{ Session::get('message') }}
		<ul>
			@foreach($errors->all() as $error)
				<li> {{ $error }} </li>
			@endforeach
		</ul>
	</div>
	@endif
		<div class="col-md-3"> </div>
		<div class="col-md-6 well">
		{{ Form::open(array('url'=>'changepassword')) }}
				<legend>Change Password for <span class=""> {{ Auth::user()->name }} </span></legend>

				<div class="form-group">
					<label for="New">New Password</label>
					<input type="password" required class="form-control" name="password" id="new_password" placeholder="New Password">
				</div>
				<div class="form-group">
					<label for="New">Confirm New Password</label>
					<input type="password" required class="form-control" name="password_confirmation" id="confirm_password" placeholder="Confirm Password">
				</div>
			
				
			
				<button type="submit" class="btn btn-primary">Submit</button>
			{{ Form::close() }}
		</div>
	</div>
</section>	
@stop