@extends('layouts.main')
@section('content')
<section class="event">
  <div class="container">
  <div class="col-md-3"></div>
 	<div class="col-md-6">
 	 
 		@if(Session::has('message'))
 			<div class="alert alert-danger">
 		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
 		<strong>Opps!</strong>
 			{{ Session::get('message') }}
 		@endif
 		@if($errors->has())
 		<ul>
 			@foreach($errors->all() as $error )
 				<li> {{ $error }} </li>
 			@endforeach
 		</ul>
 		</div>
 		@endif

 	<blockquote>
 		<b>Create an Account</b> to get full access!
 	</blockquote>
	   {{ Form::open(array('url'=>'user')) }}
	            <!-- <div class="row">
	                <div class="col-xs-12 col-sm-6 col-md-6">
	                    <div class="input-group">
	                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
	                        <input type="text" class="form-control" placeholder="Name" tabindex="1">
	                    </div>
	                </div>
	                <div class="col-xs-12 col-sm-6 col-md-6">
	                    <div class="input-group">
	                        <span class="input-group-addon"><i class="fa fa-key"></i></span>
	                        <input type="password" name="email" id="email" class="form-control " placeholder="Password" tabindex="2">
	                    </div>
	                </div>
	            </div> -->
	            <div class="input-group">
	                <span class="input-group-addon"><i class="fa fa-user"></i></span>
	                {{ Form::text('name',null,array('placeholder'=>'Name','class'=>'form-control','tabindex'=>'1','id'=>'name','required'=>'required')) }}
	            </div> 
	            <br>
	            <div class="input-group">
	                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
	                {{ Form::email('email',null,array('placeholder'=>'email','class'=>'form-control','tabindex'=>'2','id'=>'email','required'=>'required')) }}
	            </div> 
	            <br>
	            <div class="input-group">
	                <span class="input-group-addon"><i class="fa fa-paw"></i></span>
	                {{ Form::password('password',array('placeholder'=>'Password','class'=>'form-control','tabindex'=>'2','id'=>'password','required'=>'required')) }}
	            </div> 
	            <br>
	            <div class="input-group">
	                <span class="input-group-addon"><i class="fa fa-bank"></i></span>
	                {{ Form::text('college',null,array('placeholder'=>'College Name','class'=>'form-control','tabindex'=>'4','id'=>'college','required'=>'required')) }}
	            </div>  
	            <br>
	            <div class="input-group">
	                <span class="input-group-addon"><i class="fa fa-phone-square"></i></span>
	                {{ Form::text('mobile',null,array('placeholder'=>'mobile','class'=>'form-control','tabindex'=>'5','id'=>'mobile','required'=>'required')) }}
	            </div> 
	            <br>    
	            <!-- <div class="form-group row">
	                <div class="col-xs-12 col-sm-6 col-md-6">
	                    <div class="input-group">
	                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
	                        <input type="password" class="form-control" placeholder="Password" tabindex="1">
	                    </div>
	                </div>
	                <div class="col-xs-12 col-sm-6 col-md-6">
	                    <div class="input-group">
	                        <span class="input-group-addon"><i class="fa fa-key"></i></span>
	                        <input type="password" name="confirmPassword" id="confirmPassword" class="form-control " placeholder="Confirm Password" tabindex="2">
	                    </div>
	                </div>
	            </div> -->
	            <div class="row">
	                <div class="col-xs-12 col-md-6"><input type="submit" value="Create Account" class="btn btn-success btn-block " tabindex="7"></div> <div class="col-md-1" style="padding-top:7px;"><b >Or</b></div> 
	                <div class="col-xs-12 col-md-5"><a href="{{ URL::to('login/fb') }}" class="btn btn-info btn-block "> <span class="fa fa-facebook-square"></span> &nbsp Sign In</a></div>
	            </div>
	            
	            <hr class="colorgraph">
	            <div class="row text-center">
	                <a href="{{URL::to('signin')}}"> Go back to Sign In</a>
	            </div>
	       {{ Form::close() }}
	        </div>
  	</div>
</section>
@stop