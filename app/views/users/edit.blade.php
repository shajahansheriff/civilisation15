@extends('layouts.main')

@section('content')
<div class="container">
@if(Session::has('message'))
<?php $session = FlashMessage::get(Session::has('errors')) ?>
<div class="alert  {{ $session['class'] }}">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <strong> {{ $session['title'] }}</strong> {{ Session::get('message') }}
      @if(Session::has('errors'))
      <br>
      <b>Errors:</b> <ul>
        @foreach($errors->all() as $error)
        <li> {{$error }} </li>
        @endforeach
      </ul>
      @endif
		</div>
@endif

      <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">Edit, {{ $user->name }}                
              					<span class="pull-right">
              					<a href="{{ URL::to('user/'.$user->cid)  }}" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-arrow-left"></i></a>
                        		</span>
                            </h3>

            </div>
            <div class="panel-body">
              <div class="row">
              <div class="col-md-3 col-lg-3 " align="center"><a href="#profilePic" data-toggle="modal">
              @if(preg_match('/users/',$user->image))
          		{{ HTML::image($user->image,'user image',array('class'=>'img-circle img-responsive','id'=>'mainImage')) }}	
          	  @else 
          	  <img alt="User Pic" id="mainImage" src="{{ ProfilePic::getProfilePic($user->image) }}"  class="img-circle img-responsive">
          	  @endif	
               
              </a>
              </div>
          	
              
                <div class=" col-md-9 col-lg-9 "> 
            <br>
                	{{ Form::open(array('url'=>'user/'.$user->cid,'method'=>'put','files'=>true)) }}
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" name="name" id="email" required class="form-control" placeholder="name" value="{{ $user->name }}" tabindex="1">
            </div> 
            <br>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="email" name="email" id="email" required class="form-control" placeholder="email" value="{{ $user->email }}" tabindex="2">
            </div> 
            <br>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-paw"></i></span>
                <input type="password" name="password" id="password" required class="form-control" value="{{ $user->password }}" disabled placeholder="Password" tabindex="3">

            </div> 
            <br>
            <div class="input-group">
            <b> Gender: &nbsp&nbsp&nbsp&nbsp</b> 
            <label class="radio-inline">
            	<input type="radio" tabindex="4" name="gender" <?php echo $check = ($user->gender=='Male') ? 'checked' : '' ;?> value="male"> Male
            </label>
            <label class="radio-inline">
            	<input type="radio" name="gender" <?php echo $check = ($user->gender=='female') ? 'checked' : '' ;?> value="female"> Female
            </label>
            </div>
            <br>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-graduation-cap"></i></span>
                <input type="text" name="department" id="department" required class="form-control" value="{{$user->department}}" placeholder="Department" tabindex="5">
            </div> 
            <br>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-bank"></i></span>
                <input type="text" name="college" id="college" required class="form-control" value="{{$user->college}}" placeholder="college" tabindex="6">
            </div>
            <br>
            <div class="input-group">
            <b> Year: &nbsp&nbsp&nbsp&nbsp</b> 
 
            @for($i=1; $i<5; $i++)
             <?php $check = ($user->year == $i) ? true : false; ?>
              <label class="radio-inline">
                {{ Form::radio('year',$i,$check,array('tabindex'=>$i+3)) }} {{$i}}
              </label>
              @endfor
            </div>
            <br>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                <input type="text" name="city" id="department" required class="form-control" value="{{$user->city}}" placeholder="Location" tabindex="7">
            </div> 
            <br>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone-square"></i></span>
                <input type="text" name="mobile" id="mobile" required class="form-control" value="{{$user->mobile}}" placeholder="Mobile No." tabindex="8">

            </div>
            <br>
            <a href="{{ URL::to('changepassword') }}"> Change Password</a>
            <hr class="colorgraph">
            <div class="row">
                <div class="col-xs-12 col-md-12"><input type="submit" value="Save Changes" class="btn btn-success btn-block " tabindex="9"></div>
            </div>

            
            {{ Form::close() }}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<div class="modal fade" id="profilePic">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Change Profile Image</h4>
			</div>
			<div class="modal-body">
			{{ Form::open(array('url'=>'user/image','files'=>true)) }}
			<center>
				<div class="fileinput fileinput-new" data-provides="fileinput">
  <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
  <div>
    <span class="btn btn-default btn-file"><span class="fileinput-new">Browse image</span><span class="fileinput-exists">Change</span>{{ Form::file('image',array('id'=>'image')) }}</span>
    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
  </div>
</div>
</center>

			</div>
			<div class="modal-footer">
				<button type="button"  class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" id="uploadImage" class="btn btn-success">Upload Image</button>
				{{ Form::close() }}
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@stop