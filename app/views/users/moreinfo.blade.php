@extends('layouts.main')
@section('content')
<div class="container">
@if(Session::has('message'))
<?php $session = FlashMessage::get(Session::has('errors')); ?>
<div class="alert  {{ $session['class'] }}">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <strong> {{ $session['title'] }}</strong> {{ Session::get('message') }}
      @if(Session::has('errors'))
      <br>
      <b>Errors:</b> <ul>
        @foreach($errors->all() as $error)
        <li> {{$error }} </li>
        @endforeach
      </ul>
      @endif
		</div>
@endif

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
			<div class="panel panel-info">
				<div class="panel-heading">
					<blockquote>
						<b>Hello!</b> {{ Auth::user()->name }},
						<small>Please complete profile details to know more about.</small> 
					</blockquote>
				</div>
				<div class="panel-body">
					<div class="row">
						
					
					
					<div class=" col-md-12 col-lg-12 ">
						<br>
						{{ Form::open(array('url'=>'completeprofile')) }}					<br>
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-graduation-cap"></i></span>
							{{ Form::text('department',null,array('class'=>'form-control','tabindex'=>'1','required'=>'required','placeholder'=>'Department')) }}
						</div>
						<br>
						<div class="input-group">
							<b> Year: &nbsp&nbsp&nbsp&nbsp</b>
							
							@for($i=1; $i<5; $i++)
							<label class="radio-inline">
								{{ Form::radio('year',$i,false,array('tabindex'=>$i+1)) }} {{$i}}
							</label>
							@endfor
						</div>
						<br>
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
							{{ Form::text('city',null,array('class'=>'form-control','tabindex'=>'6','required'=>'required','placeholder'=>'Location')) }}
							
						</div>
						<br><div class="input-group">
			            <b> Gender: &nbsp&nbsp&nbsp&nbsp</b> 
			            <label class="radio-inline">
			            	{{ Form::radio('gender','male',false,array('tabindex'=>'7')) }} Male
			            </label>
			            <label class="radio-inline">
			            	{{ Form::radio('gender','female',false,array('tabindex'=>'8')) }} Female
			            </label>
			            <label class="radio-inline">
			            	<!-- <input type="radio" name="gender"  value="female"> Female -->
			            </label>
			            </div>
			            <br>
						<hr class="colorgraph">
						<div class="row">
							<div class="col-xs-12 col-md-12"><input type="submit" value="Save Changes" class="btn btn-success btn-block " tabindex="9	"></div>
						</div>
						
						{{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@stop