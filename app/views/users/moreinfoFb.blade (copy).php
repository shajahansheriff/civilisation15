@extends('layouts.main')
@section('content')
<div class="container">
	@if(Session::has('message'))
	<?php $session = FlashMessage::get(Session::has('errors')); ?>
		<div class="alert {{ $session['class'] }}">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>{{ $session['title'] }}</strong> {{ Session::get('message') }}		
		
		@if(Session::has('errors'))
		<ul>
					@foreach($errors as $error)
					<li>{{ $error }}</li>
					@endforeach
		</ul>
		@endif
		</div>
	@endif

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
			<div class="panel panel-info">
				<div class="panel-heading">
					<blockquote>
						<b>Hello!</b> {{ Auth::user()->name }}. <small>Please complete your profile details.</small>
					</blockquote>
				</div>
				<div class="panel-body">
					<div class="row">
						
					
					
					<div class=" col-md-12 col-lg-12 ">
						<br>
						{{ Form::open(array('url'=>'completeprofilefb','method'=>'post')) }}	
						
						<label> Password for Civilisation </label><small class="text-muted">
							(Next Time you can login either using fb or C-login)
						</small>
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-graduation-cap"></i></span>
							<input type="password" name="department" id="department" required class="form-control" value="{{$user->department}}" placeholder="Password (atleast 6 characters)" tabindex="5">
						</div>
						<br>
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-graduation-cap"></i></span>
							<input type="text" name="department" id="department" required class="form-control" value="{{$user->department}}" placeholder="Department" tabindex="5">
						</div>
						<br>
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-bank"></i></span>
							<input type="text" name="college" id="college" required class="form-control" value="{{$user->college}}" placeholder="college" tabindex="6">
						</div>
						<br>
						<div class="input-group">
							<b> Year: &nbsp&nbsp&nbsp&nbsp</b>
							<?php
								$i = 1;
								while ($i <= 4) {
							echo'<label class="radio-inline">
												<input tabindex="7" type="radio"';
												echo $check = ($user->year == $i) ? "checked" : "" ;
												echo '" name="year" id="inlineRadio2" value="'.$i.'">'.$i.'
											</label>';
											$i++;
								}
							?>
						</div>
						<br>
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
							<input type="text" name="city" id="city" required class="form-control" value="{{$user->city}}" placeholder="Location" tabindex="7">
						</div>
						<br>
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-phone-square"></i></span>
							<input type="text" name="mobile" id="mobile" required class="form-control" value="{{$user->mobile}}" placeholder="Mobile No." tabindex="8">
						</div>
						<br>
						<hr class="colorgraph">
						<div class="row">
							<div class="col-xs-12 col-md-12"><input type="submit" value="Save Changes" class="btn btn-success btn-block " tabindex="9"></div>
						</div>
						
						{{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@stop