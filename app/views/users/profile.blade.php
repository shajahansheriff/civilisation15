@extends('layouts.main')

@section('content')
<section class="profile">

	<!-- <div class="container">
	<div class="col-md-2"> </div>
	<div class="col-md-6">
		<h3>About you, <b>{{ Auth::user()->name }} </b></h3>
		<table class="table">
			<thead>
				<tr>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Name: </td>
					<td>{{ Auth::user()->name }}</td>
				</tr>
				<tr>
					<td>email: </td>
					<td>{{ Auth::user()->email }}</td>
				</tr>
				<tr>
					<td>College: </td>
					<td>{{ Auth::user()->college }}</td>
				</tr>
				<tr>
					<td>Department: </td>
					<td>{{ Auth::user()->department }}</td>
				</tr>
				<tr>
					<td>Year: </td>
					<td>{{ Auth::user()->year }}</td>
				</tr>
				<tr>
					<td>City: </td>
					<td>{{  Auth::user()->city }}</td>
				</tr>
				<tr>
					<td>Contact: </td>
					<td>{{  Auth::user()->mobile }}</td>
				</tr>
			</tbody>
		</table>
	</div>
	</div>
 -->

<div class="container">

      <div class="row">
      <!-- <div class="col-md-5  toppad  pull-right col-md-offset-3 ">
           <A href="edit.html" >Edit Profile</A>

        <A href="edit.html" >Logout</A>
       <br>
<p class=" text-info">May 05,2014,03:00 pm </p>
      </div> -->
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
        @if(Session::has('message'))
			<?php $session = FlashMessage::get(Session::has('errors'));?>
			<div class="alert {{ $session['class']}} ">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>{{ $session['title'] }}</strong> {{ Session::get('message') }}
			@if(Session::has('errors'))
			<ul>
				@foreach($errors as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
			
			@endif
			</div>
		@endif
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">About, {{ ucwords($user->name) }}  
                            
              				@if (Auth::user()->id == $user->id) 
              					<span class="pull-right">
              					<a href="{{ URL::to('user/'.$user->cid.'/edit')  }}" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-edit"></i></a>
                        		</span>
              				
              				@endif
                            </h3>

            </div>
            <div class="panel-body">
              <div class="row">
              <div class="col-md-3 col-lg-3 " align="center">
             
              	@if(preg_match('/users/',$user->image))
          		{{ HTML::image($user->image,'user image',array('class'=>'img-circle img-responsive','id'=>'mainImage')) }}	
	          	@else 
	          	  <img alt="User Pic" id="mainImage" src="{{ ProfilePic::getProfilePic($user->image) }}"  class="img-circle img-responsive">
	          	@endif
              
              </div>
                
                <!--<div class="col-xs-10 col-sm-10 hidden-md hidden-lg"> <br>
                  <dl>
                    <dt>DEPARTMENT:</dt>
                    <dd>Administrator</dd>
                    <dt>HIRE DATE</dt>
                    <dd>11/12/2013</dd>
                    <dt>DATE OF BIRTH</dt>
                       <dd>11/12/2013</dd>
                    <dt>GENDER</dt>
                    <dd>Male</dd>
                  </dl>
                </div>-->
                <div class=" col-md-9 col-lg-9 "> 
                <div class="table-responsive">
                	<table class="table table-condensed borderless">
                		<tbody>
                			<tr>
                				<td><span class="fa fa-graduation-cap"></span></td>
                				<td>{{Availability::hasDepartment($user->department)}}</td>
                			</tr>
                			<tr>
                				<td><span class="fa fa-bank"></span></td>
                				<td>{{Availability::hasCollege($user->college)}}</td>
                			</tr>
                			<tr>
                				<td><span class="fa fa-map-marker"></span></td>
                				<td>{{ Availability::hasLocation($user->city) }}</td>
                			</tr>
                			<tr>
                				<td><span class="fa fa-envelope"></span></td>
                				<td>{{ Availability::hasEmail($user->email) }}</td>
                			</tr>
                			<tr>
                				<td><span class="fa fa-phone-square"></span></td>
                				<td><samp>{{ Availability::hasMobile($user->mobile) }}</samp></td>
                			</tr>
                			<tr>
                				<td><span class="fa <?php  echo $class = ($user->gender=='Male') ? "fa-male" : "fa-female" ;?> fa-phone-square"></span></td>
                				<td>{{ ucfirst(Availability::hasGender($user->gender)) }}</td>
                			</tr>
                		</tbody>
                	</table>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<div class="modal fade" id="profilePic">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</section>
@stop