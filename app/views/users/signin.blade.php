@extends('layouts.main')
@section('content')
<div class="container" style="margin-bottom:30px;"> 
@if(Session::has('message'))
<div class="alert alert-danger">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>Oops!</strong>
   
    {{ Session::get('message') }}
</div>
@endif


   <div id="login-overlay" class="modal-dialog">
         <div class="col-md-2"></div>
      <div class="modal-content col-md-8">
          <div class="modal-header">
              <h4 class="modal-title" id="myModalLabel">Login to Civilisation</h4>
          </div>
          <div class="modal-body">
              <div class="row">
                  <div class="col-md-12">
                      <div class="well">
                          <form id="loginForm" method="POST" action="{{ URL::route('userSignin') }}">
                              <div class="form-group">
                                  <label for="email" class="control-label">email</label>
                                  <input type="text" class="form-control" id="email" name="email" value="" required="required" title="Please enter you username" placeholder="example@gmail.com">
                                  <span class="help-block"></span>
                              </div>
                              
                              <div class="form-group">
                                  <label for="password" class="control-label">Password</label>
                                  <input type="password" class="form-control" id="password" name="password" value="" required="required"  placeholder="Password" title="Please enter your password">
                                  <span class="help-block"></span>
                              </div>
                              <div id="loginErrorMsg" class="alert alert-error hide">Wrong username og password</div>
                              <div class="checkbox">
                                  <label>
                                      <input type="checkbox" value="true" name="remember" id="remember"> Remember login
                                  </label>
                                  <p class="help-block">(if this is a private computer)</p>
                              </div>
                              <button type="submit" class="btn btn-success btn-block">Login</button>
                              <a href="{{URL::to('login/fb')}}" class="btn btn-info btn-block"><span class="fa fa-facebook-square"></span> Login with Facebook</a>
                          </form>
                      </div>
                      <center>{{ HTML::link('user/create','Create an Account') }}</center>
                  </div>
                  <!-- <div class="col-xs-6">
                      <p class="lead">Register now for <span class="text-success">FREE</span></p>
                      <ul class="list-unstyled" style="line-height: 2">
                          <li><span class="fa fa-check text-success"></span> See all your orders</li>
                          <li><span class="fa fa-check text-success"></span> Fast re-order</li>
                          <li><span class="fa fa-check text-success"></span> Save your favorites</li>
                          <li><span class="fa fa-check text-success"></span> Fast checkout</li>
                          <li><span class="fa fa-check text-success"></span> Get a gift <small>(only new customers)</small></li>
                          <li><a href="/read-more/"><u>Read more</u></a></li>
                      </ul>
                      <p><a href="/new-customer/" class="btn btn-info btn-block">Yes please, register now!</a></p>
                  </div> -->
              </div>
          </div>
      </div>
  </div>
</div>


@stop