-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: civil15
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `eventCategories`
--

DROP TABLE IF EXISTS `eventCategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventCategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventCategories`
--

LOCK TABLES `eventCategories` WRITE;
/*!40000 ALTER TABLE `eventCategories` DISABLE KEYS */;
INSERT INTO `eventCategories` VALUES (1,'Hello','2015-01-06 01:08:04','2015-01-06 01:08:04');
/*!40000 ALTER TABLE `eventCategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `rules` text COLLATE utf8_unicode_ci NOT NULL,
  `contact` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'event engineering',0,'','rdrhzdth <b>zfrh</b><br><br>','<ul><li>zrhdtg</li><li>zdh</li><li>dth</li><li><br></li></ul>','<b>zfncgn <i>12345678</i></b><br><br>','2015-01-07 02:46:34','2015-01-07 02:46:34'),(2,'funda engineering',1,'','rdrhzdth<br>','<ul><li>zrhdtg</li><li>zdh</li><li>dth</li><li><br></li></ul>','<b>zfncgn <i>145678</i></b><br><br>','2015-01-07 03:28:14','2015-01-07 03:28:14'),(3,'Fun event',1,'','zfh<b>frzdf <i>fzhdf</i></b><i></i>﻿﻿<i></i><br><br><br>','<ul><li>GDFG</li><li>﻿zh</li><li>zh</li><li><br></li></ul>','<ol><li>zfgc</li><li>fzf</li><li>b<br></li></ol>','2015-01-07 03:34:52','2015-01-07 03:34:52'),(4,'photo',2,'','<b>Photo</b><br><ol><li>123</li><li>456</li><li>789<br></li></ol>','<ol><li>asd</li><li>zxc</li><li>qwe﻿</li></ol>','<ul><li>photo fuck<br></li></ul>','2015-01-07 03:51:16','2015-01-07 03:51:16'),(5,'Suck it',1,'','zfrdfb&lt;div class=\"fb-like\" data-href=\"<a href=\"https://www.facebook.com/CivilisationCEG&quot;\">https://www.facebook.com/CivilisationCEG\"</a>; data-width=\"150\" data-layout=\"button_count\" data-action=\"like\" data-show-faces=\"false\" <br>','&lt;div class=\"fb-like\"<a title=\"Link: https://www.facebook.com/CivilisationCEG&quot;\" href=\"https://www.facebook.com/CivilisationCEG&quot;\">/CivilisationCEG\"</a>; data-width=\"150\" data-layout=\"button_count\" data-action=\"like\" data-show-faces=\"false\" data-share=\"true\"&gt;&lt;/div&gt;﻿<br>','&lt;div class=\"fb-like\" data-href=\"<a title=\"Link: https://www.facebook.com/CivilisationCEG&quot;\" href=\"https://www.facebook.com/CivilisationCEG&quot;\">https://www.facebook.com</a>data-layout=\"button_count\" data-action=\"like\" data-show-faces=\"false\" data-share=\"true\"&gt;&lt;/div&gt;﻿<br>','2015-01-11 19:14:44','2015-01-11 19:14:44');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_12_02_040155_user_table',1),('2014_12_03_194145_menu_list_cretation',1),('2014_12_23_094747_user_profile',2),('2014_12_28_013045_Event_Category_Table',3),('2015_01_05_034237_update_table',4),('2015_01_06_043558_events_table',5),('2015_01_09_014745_alter_user_addMobile',6),('2015_01_10_182926_add_dpt-year-city_users',7),('2015_01_10_212356_add_image_users',8);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uid` bigint(20) unsigned NOT NULL,
  `access_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profiles`
--

LOCK TABLES `profiles` WRITE;
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` VALUES (1,1,'Shajahan Sheriff',647441245365066,'CAAFWSMf3HGIBAKJCXfnssAT53dvrTpXLS3jAh6JUodnIGfDZAQB2dzzISxbCWdoLww2cZAvxxxQcnE8T3e9OZCTxN6tz2ZBzxwrUv6jlWE41xQE6gFn2ExBCm6UQ7bxyIX4qhazfAJZCQrM4sZAVsZCsiCxUegGST4AZAUOBUNQRFbnjtkQe9F01UE3BxuK1ZAc2JpgrPfzxVh2iKwl44WD1ZA','','2015-01-10 19:17:59','2015-01-11 23:58:02');
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `updates`
--

DROP TABLE IF EXISTS `updates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `updates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `updates`
--

LOCK TABLES `updates` WRITE;
/*!40000 ALTER TABLE `updates` DISABLE KEYS */;
INSERT INTO `updates` VALUES (1,'Hello','2015-01-06 00:47:57','2015-01-06 00:47:57'),(2,'<b>hd</b><br><i>xfjxdg</i><br><br>','2015-01-06 00:48:36','2015-01-06 00:48:36'),(3,'<b>welcome to C\'15! /how</b><br><br>','2015-01-06 00:49:37','2015-01-06 00:49:37'),(4,'<b>welcome to C\'15! /how</b><br><br>','2015-01-06 00:51:31','2015-01-06 00:51:31'),(5,'<b>welcome to C\'15! /how</b><br><br>','2015-01-06 00:51:45','2015-01-06 00:51:45'),(6,'<b>welcom\"e to C\'15! /how</b><br><br>','2015-01-06 00:52:31','2015-01-06 00:52:31'),(7,'<b>welcom\"e to C\'15! /how</b><br><br>','2015-01-06 00:52:47','2015-01-06 00:52:47'),(8,'<b>welcom\"e to C\'15! /how</b><br><br>','2015-01-06 00:53:43','2015-01-06 00:53:43'),(9,'<b>welcom\"e to C\'15! /how</b><br><br>','2015-01-06 00:54:35','2015-01-06 00:54:35'),(10,'<b>welcom\"e to C\'15! /how</b><br><br>','2015-01-06 00:56:11','2015-01-06 00:56:11'),(11,'<b>welcom\"e to C\'15! /how</b><br><br>','2015-01-06 00:56:38','2015-01-06 00:56:38'),(12,'<b>welcom\"e to C\'15! /how</b><br><br>','2015-01-06 00:59:43','2015-01-06 00:59:43'),(13,'<b>welcom\"e to C\'15! /how</b><br><br>','2015-01-06 01:00:05','2015-01-06 01:00:05'),(14,'','2015-01-06 01:08:54','2015-01-06 01:08:54'),(15,'','2015-01-06 01:09:27','2015-01-06 01:09:27');
/*!40000 ALTER TABLE `updates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `college` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` text COLLATE utf8_unicode_ci,
  `mobile` bigint(20) NOT NULL,
  `department` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Shajahan','C154051995','shajansheriff@gmail.com','$2y$10$7pa7b6JPHhvjbcXaSWmmQO7EzS.FK88Tj/8GBWPfs8ETG1D.SV3We','qwerty',0,'2015-01-10 19:17:59','2015-01-11 23:57:54','BnhEd6Ozm6CAPlNBFRJG1pMeV8iipjlD1HoPHXJb6ZJyn2ss8uZtmORn3nx4',1234567890,'Hello','chennai',3,'users/dp/c15-grunt.png','male'),(2,'Tester','C154051996','tester@civil.com','$2y$10$.XzRSPiATrMb9.2EqbEjXeiAe4Pcl/s1d/E8RSLpQKpdIC8pOoBJG','ceg',0,'2015-01-10 19:18:16','2015-01-11 12:04:32','4gsQWXfNfnJy5ShTrIEQFxvInhR3PRb6RguCBhkQpvebUVeOJabFOeGZZXGu',1234567890,'','',0,'',''),(3,'','','','','',0,'2015-01-11 14:19:29','2015-01-11 14:19:29',NULL,0,'','',0,'users/dp/c15-grunt.png',''),(4,'Admin','C154051998','admin@civilisationceg.com','$2y$10$U/jBOuRHzyC9Cqh/tvQwRe99S7TEZI.y2njGX0zFLCBzLauhnwWP6','CEG',1,'2015-01-11 19:03:36','2015-01-11 19:03:36',NULL,1234567890,'','',0,'',''),(5,'Tester','C154051999','tester1@civil.com','$2y$10$XoD5ZMBx3u/Nsqnt7Auy3e41DNr2iK8/5BbBFOILwEG4NWeVCLjS.','CEG',0,'2015-01-11 19:09:37','2015-01-11 19:13:50','lFsDx0ezbhpdoyC0JKZumJzp73nCuC4PUq0VaBaXOOp5kcNzyVfp8tmpSgck',1234567890,'','',0,'','');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-01-12 11:34:29
