var adminApp = angular.module('adminApp',['menuService'],function($interpolateProvider)
	{
		$interpolateProvider.startSymbol('<%');
		$interpolateProvider.endSymbol('%>');
	}
);