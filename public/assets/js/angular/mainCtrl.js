adminApp.controller('menuCtrl',['$scope','$http','Menu',function($scope,$http,Menu){	
	$scope.menuData = {};

	$scope.loading = true;
	

	Menu.get().success(function(data){
		$scope.menus = data;
		console.log(data);
		$scope.loading = false;
	}).error(function(data) {
		/* Act on the event */
		console.log(data);
	});

	$scope.submitMenu = function()
	{

		$scope.loading = true;
		//$scope.menus.push($scope.menuData.name);
		Menu.save($scope.menuData).success(function(data)
		{
			$scope.menuData = {};
			if(data.success != true)
			{
				$scope.menuError = 'Atleast 2 Characters';
				console.log('error');
			}
			Menu.get().success(function(data)
			{
				$scope.menus = data;
				$scope.loading = false;
			});
		})
		.error(function(data) 
		{	
			console.log(data);
		});
	};
	
	$scope.deleteMenu = function(id) {
		$scope.loading = true;

		Menu.destroy(id).success(function(data){
			Menu.get().success(function(getdata){
				$scope.menus = getdata;
				$scope.loading = false;
			});
		});
	};

}]);