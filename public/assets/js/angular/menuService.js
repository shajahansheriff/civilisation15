angular.module('menuService', []).factory('Menu', ['$http',
   
    function($http) {
        return {
            get: function() {
                return $http.get('/menus');
            },

            save: function(menuData) {
                return $http({
                    method: 'POST',
                    url: '/menus',
                    headers: {
                        'Content-type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param(menuData)
                });

            },

            destroy: function(id) {
                return $http.delete('/menus/' + id);
            }
        }
    }
]);