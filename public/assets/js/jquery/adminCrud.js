/**
 * Category Part
 */
//create category
$('#createCategory').click(function(event) {
	/* Act on the event */
	if (isValidCategory(getCategory())) {
	$.ajax({
		url: 'eventCategory',
		type: 'POST',
		dataType: 'text',
		data: {name: getCategory},
		success:function(data){
			$("#listEvent:last").after(data);
		}
	})
	.done(function() {
		console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
} 
	else {
	alert('Category Name Must be atleast 2 Characters');
}

});
function isValidCategory(name){
	if (name != null && name.length >= 2) {
		return true;
	};
	return false;
}
function getCategory () {
	var name = $('#categoryName').val();
	return name;
}
$('#categoryName').load(function(event) {
	alert($(this).val());

});

//end of create category
//delte category 
$('.deleteCategory').click(function(event) {
	var id = $(this).attr('id');
	$.ajax({
		url: 'eventCategory/'+id,
		type: 'DELETE',
		dataType: 'text',
		data: {categoryId: id},
	})
	.done(function() {
		console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
});
/**
 * End of -/Category 
 */