/**
 * Description Editor
 */

$(window).load(function() {
	/* Act on the event */
	$('.alert').hide();
});
$('#descEditor').wysihtml5();

$('#saveDescEditor').click(function(event) {
	var value =  $('#descEditor').val();
	$.ajax({
		url: 'eventeditor',
		type: 'POST',
		dataType: 'html',
		data: {description: value,type:0},
		success: function(value){
			value = jQuery.parseJSON(value);
			for (var i = value.length - 1; i >= 0; i--) {
				$('#showDesc').append(value[i].description);
			};
		}
	})
	.done(function() {
		console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
});

/**
 * Event Rules
 * @param  {[type]} event)   {	var                    value         [description]
 * @param  {[type]} success: function(value){			value [description]
 * @return {[type]}          [description]
 */

$('#rulesEditor').wysihtml5();
$('#contactEditor').wysihtml5();
$('#saveRulesEditor').click(function(event) {
	var value =  $('#rulesEditor').val();
	$.ajax({
		url: 'eventeditor',
		type: 'POST',
		dataType: 'html',
		data: {rules: value,type:1},
		success: function(value){
			value = jQuery.parseJSON(value);
			for (var i = value.length - 1; i >= 0; i--) {
				$('#showRules').append(value[i].description);
			};
		}
	})
	.done(function() {
		console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
});

$('#saveEvent').click(function(event) {
	/* Act on the event */
	var name = $('#eventName').val();
	var description = $('#descEditor').val();
	var rules = $('#rulesEditor').val();
	var contact = $('#contactEditor').val();
	var category = $('select[name=category]').val();
	$.ajax({
		url: 'eventeditor',
		type: 'POST',
		dataType: 'html',
		data: {name: name, description: description, rules: rules, contact: contact, category: category},	
		success: function(value){
			$('.alert').show();
		}
	})
	.done(function() {
		console.log("Event success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
	
});