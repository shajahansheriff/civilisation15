//loading image
$(window).ajaxStart(function() {
	$('#eventLoading').show('fast');
});
$(window).ajaxComplete(function(event, xhr, settings) {
	$('#eventLoading').hide('fast');
});
// end of loading image
/**
 * Activate the first element in the list
 * @param  {[type]} )        {	var                  id   [take the event id]
 * @param  {[type]} success: function(value){			var data [description]
 * @return {[type]}          [description]
 */
$(window).load(function() {
	var id = $('.list-group-item:first').attr('id');
	$.ajax({
		url: 'events/'+id,
		type: 'GET',
		dataType: 'html',
		data: {eventId: id},
		success: function(value){
			var data = jQuery.parseJSON(value);
			$('#description').html(data.description);
			$('#rules').html(data.rules);
			$('#contact').html(data.contact);
			$('#subscribe').attr('event', data.id);
			//check the status of the subscription
			$.get('subscribe/'+data.id, function(result) {
				isSubscribed(result);
			});
		}
	})
	.done(function() {
		console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
});

/**
 * getting individual event description, rules and contact
 * @param  {[type]} event)   {	var                   id   [description]
 * @param  {[type]} success: function(result){			var data [description]
 * @return {[type]}          [description]
 */
$('.list-group-item').click(function(event) {
	var id = $(this).attr('id');
	$.ajax({
		url: 'events/'+id,
		type: 'GET',
		dataType: 'html',
		data: {eventId: id},
		success: function(result){
			var data = jQuery.parseJSON(result);
			$('#description').html(data.description);
			$('#rules').html(data.rules);
			$('#contact').html(data.contact);
			$('#subscribe').attr('event', data.id);
			$('#descTab').click();
			//check the status of the subscription
			$.get('subscribe/'+data.id, function(result) {
				isSubscribed(result);
			});
		}
	})
	.done(function() {
		console.log("success");
	})
	.fail(function() {
		console.log("error occured");
	})
	.always(function() {
		console.log("complete");
	});
});
/**
 * [isSubscribed is used to assign the property for the subscribe button]
 * @param  {[type]}  data [description]
 * @return {Boolean}      [description]
 */
function isSubscribed(data){
	if (data == 'YES') {
		unSubscribe();
	}
	else {
		subscribe();	
	}

}

/**
 * Subscribe event 
 * @param  {[type]} event)   {	var        stage            [description]
 * @param  {[type]} success: function      (result){				if (result       [description]
 * @return {[type]}          [description]
 */
$('#subscribe').click(function(event) {
	var stage = $(this).attr('stage');
	var status = $(this).attr('status');
	var eventId = $(this).attr('event');
	//User is SignedIn and the event is Unsubscribed
	if (stage == 1 && status == 0) {
		$.ajax({
			url: 'subscribe',
			type: 'POST',
			dataType: '',
			data: {eventId: eventId},
			success: function (result){
				if (result == 'YES') {
					unSubscribe();
				}
				else subscribe();	
			
			}
		})
		.done(function() {
			console.log("success");
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	}
	//user is signedIn and the event is already subscribed and function to unsubscribe
	else if (stage == 1 && status == 1) {
		$.ajax({
			url: 'subscribe/'+eventId,
			type: 'DELETE',
			dataType: 'html',
			data: {event_id: eventId},
			success: function(result){
				if (result == 'YES') {
					subscribe();
				}
				else unSubscribe();
			}
		})
		.done(function() {
			console.log("success");
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	}; 
});
/**
 * [unSubscribe is used to set the property to subscribe button Once the event is subscribed]
 * @return {[type]} [description]
 */
function unSubscribe(){
	$('#subscribe').removeClass('btn-primary').html('Unsubscribe').addClass('btn-default').attr('status', '1');;
	$('#subscribe').attr('data-original-title', 'Click to Unsubscribe');
}
/**
 * [subscribe is used to set property if the event is not subscribed]
 * @return {[type]} [description]
 */
function subscribe(){
	$('#subscribe').attr('data-original-title', 'Click to Subscribe').addClass('btn-primary').attr('title', 'Click to Subscribe').html('Subscribe').attr('status', '0');;
}




